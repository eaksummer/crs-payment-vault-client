
var expect = chai.expect;
var assert = chai.assert;

describe("TravelaniumVS", function () {

    describe("constructor", function () {
//
//        it("should have a default name", function () {
//            var vaule = new TravelaniumVS();
//            expect(vaule).to.not.equal('undefined');
//        });


//        it("should have a  calls original function with right this and args", function () {
//
//            var config = {
//                paymentVaultBaseUrl: "http://localhost:8080/vault",
//                publicKey: "0851DFB34B4C3B3976E984E96A9B4D2EE7CE87094BF62EA09E364E10AC275B8B"
//            };
//
//            var tokenInfo = {
//                key: "123456789123456789"
//            };
//
//            var vaule = new TravelaniumVS(easyXDM, CryptoJS, config);
//
//            var callback = sinon.spy();
//            var proxy = vaule.test(tokenInfo, callback);
//
//            sinon.assert.calledOnce(callback);
//        });
//
////
        it('should have a  callback function only once', function () {

            var config = {
                paymentVaultBaseUrl: "http://localhost:8080/vault",
                publicKey: "0851DFB34B4C3B3976E984E96A9B4D2EE7CE87094BF62EA09E364E10AC275B8B"
            };

            var cardInformation = {property_id: "29",
                propertygroup_id: null,
                holder_name: "test test",
                number: "5422882800700007",
                brand: "VISA",
                expiration_month: "05",
                expiration_year: "2020",
                security_code: "999",
                checkout: "2020-11-05"
            };

            var vaule = new TravelaniumVS(easyXDM, CryptoJS, config);
            console.log(vaule);
            // var callback = sinon.stub().returns(42);

            var callback = sinon.spy();

            var proxy = vaule.submitCard(cardInformation, callback);
            sinon.assert.calledOnce(callback);
        });

//        it('should have returns error UNAUTHORIZED code', function () {
//
//            var config = {
//                paymentVaultBaseUrl: "http://localhost:8080/vault",
//                publicKey: "0851DFB34B4C3B3976E984E96A9B4D2EE7CE87094BF62EA09E364E10AC275B8B"
//            };
//
//            var cardTempToken = "xxx.eyJBdXRob3JpemF0aW9uIjpbInJlYWQgNCBkaWdpdCIsInJlYWQiLCJtb2RpZnkgY2hlY2tvdXQiLCJjaGFuZ2Ugc3RhdHVzIl0sInN1YiI6InJlZnJlc2giLCJhdWQiOiJDUlMgVkFVTFQiLCJpc3MiOiJUcmF2ZWxhbml1bSBDby4sIEx0ZCIsImV4cCI6MTUyNjE0NDQwMCwiaWF0IjoxNTE2NjE0MjUyLCJqdGkiOiI1YTY1YjI2YzM4MGY0NTAyNDg0NzZlYWIiLCJjbGllbnRfaWQiOjI5LCJjYXJkX2lkIjoiNWE2NWIyNmMzODBmNDUwMjQ4NDc2ZWFiIn0.EfCUKWk45kCppeIS8LIAykjvTAtS1J_c4UUMl0whTt5UtW-EqZLTSpJmnP_YtHUevi2KEmcaUqEH4wReDKqlaVSHbd8VDU7o5k1dGsujNfKpSMEDGSQmFZMtumzv9XETZNPnlNpQGGVCCoPHSE8mlzpjLEF4NS0u7EXtoLkmi0-NTKgydVtdm0mlfycA67T3OmNBAmhF9jcOtdg5cYhEiKleflM3bCQvJH8375Xn9Emqr-41kfb5poz7I77iMkxe7w32ppIfVdu9mnfZXeqlHkxqtYJ0DkiD3kU8pSIWpVrDzYQzYh17Jkb3Oo4qjFbS5vdc5phdnP36PkWEIbO3sg";
//
//            var vaule = new TravelaniumVS(easyXDM, CryptoJS, config);
//
//            var callback = sinon.spy();
//            ;
//
//            var proxy = vaule.getCard(cardTempToken, callback, "2");
//            console.log(callback.called);
//            assert(callback.called);
//
//
//        });

    });


});