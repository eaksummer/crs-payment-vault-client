var TravelaniumVS = function TravelaniumVS(easyXDM, CryptoJS, config) {
    
    'use strict';

    //Set config
    var paymentVaultBaseUrl = "http://localhost:8080";
    var publicKey = "";

    if (config && config.paymentVaultBaseUrl) {
        paymentVaultBaseUrl = config.paymentVaultBaseUrl;
    }

    if (config && config.publicKey) {
        publicKey = config.publicKey;
    }

    //Make request
    var makeRequest = function makeRequest(paymentVaultBaseUrl, requestUrl, method, accessToken, publicKey, data, key) {

        var remoteUrl = getRemoteUrl(paymentVaultBaseUrl);
        var xhr = createXMLHttpRequest(remoteUrl);

        return new Promise(function (resolve, reject) {
            xhr.request({
                url: requestUrl,
                method: method,
                authorization: "Bearer " + accessToken,
                apiKey: publicKey,
                data: data
            }, function (response) {
                var returnVal = { response: response, key: key };
                resolve(returnVal);
            }, function (error) {
                var returnVal = { response: error, key: key };
                reject(returnVal);
            });
        });
    };

    //Create EasyXDM
    function createXMLHttpRequest(remoteUrl) {

        var xhr = new easyXDM.Rpc({
            remote: remoteUrl
        }, {
            remote: {
                request: {}
            }
        });

        return xhr;
    }

    function getRemoteUrl(baseUrl) {
        return baseUrl + "/cors/index.html";
    }

    function getOauth2Url(baseUrl) {
        return baseUrl + "/api/oauth2/token";
    }

    function getCardUrl(baseUrl) {
        return baseUrl + "/api/cards/card";
    }

    //Get access token
    var getAccessToken = function getAccessToken(paymentVaultBaseUrl, publicKey) {

        var oauth2Url = getOauth2Url(paymentVaultBaseUrl);
        var request = makeRequest(paymentVaultBaseUrl, oauth2Url, "GET", "", publicKey, "", "");
        return request;
    };

    //Submit credit card
    var postNewCreditCard = function postNewCreditCard(paymentVaultBaseUrl, publicKey, data, accessToken) {

        var cardUrl = getCardUrl(paymentVaultBaseUrl);
        var request = makeRequest(paymentVaultBaseUrl, cardUrl, "POST", accessToken, publicKey, data);
        return request;
    };

    //Extract token and encrypt credit card
    var extractTokenAndEncryptCreditCard = function extractTokenAndEncryptCreditCard(cardInfo, returnVal) {

        if (returnVal && returnVal.response) {
            if (returnVal.response.status === 200) {
                var tokenInfo = JSON.parse(returnVal.response.data);
                return encrypt(cardInfo, tokenInfo);
            }
        }
    };

    //Encrypt
    var encrypt = function encrypt(data, tokenInfo) {

        return new Promise(function (resolve, reject) {

            var key = CryptoJS.enc.Utf8.parse(tokenInfo.key);
            var iv = CryptoJS.enc.Utf8.parse(tokenInfo.key.substring(0, 16));
            var msg = JSON.stringify(data);
            var encrypted = CryptoJS.AES.encrypt(msg, key, { iv: iv });

            resolve({ cd: encrypted.toString(), tokenInfo: tokenInfo });
        });
    };

    //Get actual access token
    var refreshToken = function refreshToken(paymentVaultBaseUrl, publicKey, accessToken) {

        var oauth2Url = getOauth2Url(paymentVaultBaseUrl);
        var refreshTokenUrl = oauth2Url + "/refresh/read";
        var request = makeRequest(paymentVaultBaseUrl, refreshTokenUrl, "GET", accessToken, publicKey, "", "");
        return request;
    };

    //Extract token
    var extractToken = function extractToken(returnData) {

        return new Promise(function (resolve, reject) {

            if (returnData && returnData.response) {
                if (returnData.response.status === 200) {
                    var tokenInfo = JSON.parse(returnData.response.data);
                    resolve({ tokenInfo: tokenInfo });
                } else {
                    reject({ response: returnData.response });
                }
            }
        });
    };

    //Submit credit card
    var getCreditCard = function getCreditCard(paymentVaultBaseUrl, publicKey, accessToken, key) {

        var cardUrl = getCardUrl(paymentVaultBaseUrl);
        var request = makeRequest(paymentVaultBaseUrl, cardUrl, "GET", accessToken, publicKey, "", key);
        return request;
    };

    //Extract and decrypt credit card
    var extractAndDecryptCreditCard = function extractAndDecryptCreditCard(returnVal) {

        if (returnVal && returnVal.response) {
            if (returnVal.response.status === 200) {
                var tokenInfo = JSON.parse(returnVal.response.data);
                return decrypt(tokenInfo.cd, returnVal.key);
            }
        }
    };

    //Encrypt 
    var decrypt = function decrypt(dataStr, originalKey) {

        return new Promise(function (resolve, reject) {

            var key = CryptoJS.enc.Utf8.parse(originalKey);
            var iv = CryptoJS.enc.Utf8.parse(originalKey.substring(0, 16));

            var decrypted = CryptoJS.AES.decrypt(dataStr, key, { iv: iv });
            var str = decrypted.toString(CryptoJS.enc.Utf8);
            resolve({ response: { data: str, status: 200 } });
        });
    };

    console.log("Prepare object...");
    var internalObject = {

        submitCard: function submitCard(cardInfo, callback) {

            var createToken = getAccessToken(paymentVaultBaseUrl, publicKey);
            createToken.then(function (returnVal) {
                return extractTokenAndEncryptCreditCard(cardInfo, returnVal);
            }).then(function (data) {
                return postNewCreditCard(paymentVaultBaseUrl, publicKey, {cd: data.cd}, data.tokenInfo.access_token);
            }).then(function (data) {
                return callback(data.response);
            })["catch"](function (err) {
                return callback(err.response);
            });
        },

        getCard: function getCard(cardTempToken, callback, index) {

            var tokenInfo = refreshToken(paymentVaultBaseUrl, publicKey, cardTempToken);
            tokenInfo.then(function (returnVal) {
                return extractToken(returnVal);
            }).then(function (data) {
                return getCreditCard(paymentVaultBaseUrl, publicKey, data.tokenInfo.access_token, data.tokenInfo.key);
            }).then(function (data) {
                return extractAndDecryptCreditCard(data);
            }).then(function (data) {
                return callback(data.response, index);
            })["catch"](function (err) {
                return callback(err.response, index);
            });
        }
    };

    return internalObject;
};