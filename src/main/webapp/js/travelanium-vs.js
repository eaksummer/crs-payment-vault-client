
'use strict';

// Config: public key, service base url
// Dependency: EasyXdm, CryptoJS
// Process:
//   - Create: Get token, encrypt credit card, post card
// Test case:
//   - Get token success
//      - response status of request authen
//      - verify call encript once
//      - verify easy xdm call request(post) once.
//      - asert final response not null.
//   - Get token fail
//      - never call encript cd
//      - never post credit card with easy xdm.
//      - pass response to call back

var TravelaniumVS = (function (easyXDM, CryptoJS, config) {

    //Set config
    var paymentVaultBaseUrl = "http://localhost:8080";
    var publicKey = "";

    if (config && config.paymentVaultBaseUrl) {
        paymentVaultBaseUrl = config.paymentVaultBaseUrl;
    }

    if (config && config.publicKey) {
        publicKey = config.publicKey;
    }


    //Make request
    var makeRequest = function (paymentVaultBaseUrl, requestUrl, method, accessToken, publicKey, data, key) {

        var remoteUrl = getRemoteUrl(paymentVaultBaseUrl);
        var xhr = createXMLHttpRequest(remoteUrl);

        return new Promise(function (resolve, reject) {
            xhr.request({
                url: requestUrl,
                method: method,
                authorization: "Bearer " + accessToken,
                apiKey: publicKey,
                data: data
            }, function (response) {
                var returnVal = {response: response, key: key};
                resolve(returnVal);
            }, function (error) {
                var returnVal = {response: error, key: key};
                reject(returnVal);
            });
        });
    };
    

    //Create EasyXDM
    function createXMLHttpRequest(remoteUrl) {

        var xhr = new easyXDM.Rpc({
            remote: remoteUrl
        }, {
            remote: {
                request: {}
            }
        });

        return xhr;
    }

    function getRemoteUrl(baseUrl) {
        return baseUrl + "/cors/index.html";
    }

    function getOauth2Url(baseUrl) {
        return baseUrl + "/api/oauth2/token";
    }

    function getCardUrl(baseUrl) {
        return baseUrl + "/api/cards/card";
    }


    //Get access token
    var getAccessToken = function (paymentVaultBaseUrl, publicKey) {

        var oauth2Url = getOauth2Url(paymentVaultBaseUrl);
        var request = makeRequest(paymentVaultBaseUrl, oauth2Url, "GET", "", publicKey, "", "");
        return request;
    };


    //Submit credit card
    var postNewCreditCard = function (paymentVaultBaseUrl, publicKey, data, accessToken) {

        var cardUrl = getCardUrl(paymentVaultBaseUrl);
        var request = makeRequest(paymentVaultBaseUrl, cardUrl, "POST", accessToken, publicKey, data);
        return request;
    };


    //Extract token and encrypt credit card
    var extractTokenAndEncryptCreditCard = function (cardInfo, returnVal) {

        if (returnVal && returnVal.response) {
            if (returnVal.response.status === 200) {

                var tokenInfo = JSON.parse(returnVal.response.data);
                return encrypt(cardInfo, tokenInfo);
            } 
        }
    };


    //Encrypt
    var encrypt = function (data, tokenInfo) {
        
        return new Promise(function (resolve, reject) {

            var key = CryptoJS.enc.Utf8.parse(tokenInfo.key);
            var iv = CryptoJS.enc.Utf8.parse(tokenInfo.key.substring(0, 16));

            var msg = JSON.stringify(data);
            var encrypted = CryptoJS.AES.encrypt(msg, key, {iv: iv});
            
            resolve({cd: encrypted.toString(), tokenInfo: tokenInfo});
        });
    };


    //Get actual access token
    var refreshToken = function (paymentVaultBaseUrl, publicKey, accessToken) {

        var oauth2Url = getOauth2Url(paymentVaultBaseUrl);
        var refreshTokenUrl = oauth2Url + "/refresh/read";
        var request = makeRequest(paymentVaultBaseUrl, refreshTokenUrl, "GET", accessToken, publicKey, "", "");
        return request;
    };


    //Extract token
    var extractToken = function (returnData) {

        return new Promise(function (resolve, reject) {

            if (returnData && returnData.response) {
                if (returnData.response.status === 200) {

                    var tokenInfo = JSON.parse(returnData.response.data);
                    resolve({tokenInfo: tokenInfo});

                } else {
                    reject({response: returnData.response});
                }
            }
        });
    };


    //Submit credit card
    var getCreditCard = function (paymentVaultBaseUrl, publicKey, accessToken, key) {

        var cardUrl = getCardUrl(paymentVaultBaseUrl);
        var request = makeRequest(paymentVaultBaseUrl, cardUrl, "GET", accessToken, publicKey, "", key);
        return request;
    };


    //Extract and decrypt credit card
    var extractAndDecryptCreditCard = function (returnVal) {

        if (returnVal && returnVal.response) {
            if (returnVal.response.status === 200) {

                var tokenInfo = JSON.parse(returnVal.response.data);
                return decrypt(tokenInfo.cd, returnVal.key);
            } 
        }
    };


    //Encrypt 
    var decrypt = function (dataStr, originalKey) {

        return new Promise(function (resolve, reject) {

            var key = CryptoJS.enc.Utf8.parse(originalKey);
            var iv = CryptoJS.enc.Utf8.parse(originalKey.substring(0, 16));

            var decrypted = CryptoJS.AES.decrypt(dataStr, key, {iv: iv});
            var str = decrypted.toString(CryptoJS.enc.Utf8);
            resolve({response: {data: str, status: 200}});
        });
    };


    console.log("Prepare object...");
    var internalObject = {

        submitCard: function (cardInfo, callback) {

            var createToken = getAccessToken(paymentVaultBaseUrl, publicKey);
            createToken
                    .then(returnVal => extractTokenAndEncryptCreditCard(cardInfo, returnVal))
                    .then(data => postNewCreditCard(paymentVaultBaseUrl, publicKey, {cd: data.cd}, data.tokenInfo.access_token))
                    .then(data => callback(data.response))
                    .catch(err => callback(err.response));
        },

        getCard: function (cardTempToken, callback, index) {

            var tokenInfo = refreshToken(paymentVaultBaseUrl, publicKey, cardTempToken);
            tokenInfo.then(returnVal => extractToken(returnVal))
                    .then(data => getCreditCard(paymentVaultBaseUrl, publicKey, data.tokenInfo.access_token, data.tokenInfo.key))
                    .then(data => extractAndDecryptCreditCard(data))
                    .then(data => callback(data.response,index))  //index is optional
                    .catch(err => callback(err.response, index)); //index is optional
        }
    };

    return internalObject;
});

