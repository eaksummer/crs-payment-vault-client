
'use strict';


function createToken() {

    var publicKey = document.querySelector('[data-name="x-apikey"]').value;
    var paymentVaultBaseUrl = document.querySelector('[data-name="payment-vault-base-url"]').value;

    var cardInformation = { property_id: "",
                            propertygroup_id: null,
                            holder_name: "",
                            number: "",
                            brand: "",
                            expiration_month: "",
                            expiration_year: "",
                            security_code: "",
                            checkout: ""
                        };

    //Get Value
    cardInformation.property_id = document.querySelector('[data-name="property-id"]').value;

    var propertygroup_id = document.querySelector('[data-name="propertygroup-id"]').value;
    if (propertygroup_id !== "") {
        cardInformation.propertygroup_id = propertygroup_id;
    }

    cardInformation.holder_name = document.querySelector('[data-name="holder-name"]').value;
    cardInformation.number = document.querySelector('[data-name="number"]').value;
    cardInformation.brand = document.querySelector('[data-name="brand"]').value;
    cardInformation.expiration_month = document.querySelector('[data-name="expiration-month"]').value;
    cardInformation.expiration_year = document.querySelector('[data-name="expiration-year"]').value;
    cardInformation.security_code = document.querySelector('[data-name="security-code"]').value;

    var checkout = document.querySelector('[data-name="checkout"]');
    if (checkout) {
        cardInformation.checkout = document.querySelector('[data-name="checkout"]').value;
    }

    var config = {
        paymentVaultBaseUrl: paymentVaultBaseUrl,
        publicKey: publicKey
    };
    

    var vault = TravelaniumVS(easyXDM, CryptoJS, config);
    
    console.log(vault);
    console.dir(vault);
    vault.submitCard(cardInformation, getCardTempToken);
    
    return false;
}


var getCardTempToken = function (response) {
      
    console.log("getCardTempToken");
    console.log(response);
    

    if (response) {

        if (response.status === 201) {
            
             console.log(response.status);
            
            var tokenInfo = JSON.parse(response.data);
            document.getElementById('result').innerHTML = "<pre>" + JSON.stringify(tokenInfo, undefined, 2) + "</pre>";
            document.getElementById('card-temp-token').value = tokenInfo.card_temp_token;

        } else {

            //Log error
            console.log(response);

            //Display error message
            handleErrorMessage(response);
        }
    }
};


function getCreditCardDetails() {

    //Get card temp token
    var publicKey = document.querySelector('[data-name="x-apikey"]').value;
    var paymentVaultBaseUrl = document.querySelector('[data-name="payment-vault-base-url"]').value;
    var cardTempToken = document.querySelector('[data-name="card-temp-token"]').value;

    var config = {
        paymentVaultBaseUrl: paymentVaultBaseUrl,
        publicKey: publicKey
    };

    var vault = TravelaniumVS(easyXDM, CryptoJS, config);  
    
    
    console.log(vault);
    console.dir(vault);
    
    
    vault.getCard(cardTempToken, getCreditCardTokenResult);
}


var getCreditCardTokenResult = function (response) {    

    if (response) {

        if (response.status === 200 || response.status === 201) {

            //Convert to JSON ojbect
            var creditCardInfo = JSON.parse(response.data);
            if (creditCardInfo) {

                console.log(creditCardInfo);
                document.getElementById("result-view-card").innerHTML = "<pre>" + JSON.stringify(creditCardInfo, undefined, 2) + "</pre>";
            }

        } else {

            //Log error
            console.log(response);

            //Display error message
            handleErrorMessage(response);
        }
    }
};


function handleErrorMessage(response) {

    if (response) {

        var error = JSON.parse(response.message.data);
        if (error) {
            
            var errorCode = error.Code;
            var msg = document.querySelector("#vault-error-999").innerText;
            var errorMsgID = "#vault-error-" + errorCode;

            //Get error message by id
            var element = document.querySelector(errorMsgID);
            if (element) {
                var errorMsg = element.innerText;
                if (errorMsg) {
                    msg = errorMsg;
                }
            }

            //Display error message
            alert(msg);
        }

    } else {
        console.log(response);
    }
}
